<?php

// S04: Access Modifiers and Encapsulation

// Learn how to protect object properties by using access modifiers
/* 
    3 Types of Access Modifiers

    1. public - the property or method can be accessed from everywhere. This is default. 
    
    2. private - the property or method can ONLY be accessed within the class.
    
    3. protected - the property or method can be accessed within the class and by classes derived from that class
    

*/


// Encapsulation - Wrapping up data member and method together into a single unit is called Encapsulation. Encapsulation also allows a class to change its internal implementation without hurting the overall functioning of the system. Binding the data with the code that manipulates it.

class Building {
    protected $name;
    protected $floors;
    protected $address;

    public function __construct($name, $floors, $address) {
        $this -> name = $name;
        $this -> floors = $floors;
        $this -> address = $address;
    }

        // Encapsulation - Getter
        public function getName(){
            return $this->name;
        }
        // Encapsulation - Setter
        public function setName($name){
            $this->name = $name;
        }

        // Encapsulation - Getter
        public function getFloors(){
            return $this->floors;
        }
        // Encapsulation - Setter
        private function setFloors($floors){
            $this->floors = $floors;
        }

        // Encapsulation - Getter
        public function getAddress(){
            return $this->address;
        }
        // Encapsulation - Setter
        private function setAddress($address){
            $this->address = $address;
        }
}

// “Getters” and “Setters” are object methods that allow you to control access to a certain class variables / properties. Sometimes, these functions are referred to as “mutator methods”. A “getter” allows to you to retrieve or “get” a given property. A “setter” allows you to “set” the value of a given property. 
// Can also be used to either parent or inherited class
class Condominium extends Building {

}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');
$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');